function generate() {
    if(!validateForm()){
        return false;
    };

    changeSocialIconVisibility();

    $("#cardOutput").show();
    $("#signatureName").text($("#inputFullname").val());
    $("#signatureDesignation").text($("#inputDesignation").val());
    $("#signatureEmail").text($("#inputEmail").val());
    $("#signaturePhone").text($("#inputPhone").val());
    $("#logoURL").attr("src", $("#inputLogoURL").val());
    $("#signatureLinkedIn").attr("href", $("#inputLinkedIn").val());
    $("#signatureTwitter").attr("href", $("#inputTwitter").val());
    $("#signatureFacebook").attr("href", $("#inputFacebook").val());

};

function validateForm() {
    //reset the invalid class
    $("#inputFullname, #inputDesignation, #inputEmail, #inputPhone, #inputLogoURL, #inputLinkedIn, #inputTwitter, #inputFacebook").removeClass('is-invalid');
    
    //check each required inputfield
    if ($("#inputFullname").val() === '') {
        $("#inputFullname").addClass("is-invalid").focus();
        return false;
    }

    if ($("#inputDesignation").val() === '') {
        $("#inputDesignation").addClass("is-invalid").focus();
        return false;
    }

    if ($("#inputEmail").val() === '' || isEmail($("#inputEmail").val()) == false) {
        $("#inputEmail").addClass("is-invalid").focus();
        return false;
    }

    if($("#inputPhone").val() === ''){
        $("#inputPhone").addClass("is-invalid").focus();
        return false;
    }

    if($("#inputLogoURL").val() === ''){
        $("#inputLogoURL").addClass("is-invalid").focus();
        return false;
    } else if (isURL($("#inputLogoURL").val()) == false) {
        $("#inputLogoURL").addClass("is-invalid").focus();
        $("#cardOutput").hide();
        return false;
    }

    //check each social media URL
    if ($("#inputLinkedIn").val() != '' && isURL($("#inputLinkedIn").val()) == false) {
        $("#inputLinkedIn").addClass("is-invalid").focus();
        $("#cardOutput").hide();
        return false;
    }

    if ($("#inputTwitter").val() != '' && isURL($("#inputTwitter").val()) == false) {
        $("#inputTwitter").addClass("is-invalid").focus();
        $("#cardOutput").hide();
        return false;
    }

    if ($("#inputFacebook").val() != '' && isURL($("#inputFacebook").val()) == false) {
        $("#inputFacebook").addClass("is-invalid").focus();
        $("#cardOutput").hide();
        return false;
    }

    return true;
}

function isURL(url) {
    var regex = /http[s]?:\/\/(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+/;
    if(!regex.test(url)){
        return false;
    } else {
        return true;
    }
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        return false;
    } else{
        return true;
    }
}

function changeSocialIconVisibility() {
    if($("#inputLinkedIn").val() === '') {
        $("#signatureLinkedIn").hide();
    } else {
        $("#signatureLinkedIn").show();
    }

    if($("#inputTwitter").val() === '') {
        $("#signatureTwitter").hide();
    } else {
        $("#signatureTwitter").show();
    }

    if($("#inputFacebook").val() === '') {
        $("#signatureFacebook").hide();
    } else {
        $("#signatureFacebook").show();
    }
}

function resetForm() {
    $("#cardOutput").hide();   
}